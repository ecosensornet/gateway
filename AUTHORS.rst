=======
Credits
=======

Development Lead
----------------

.. {# pkglts, doc.authors

* revesansparole, <revesansparole@gmail.com>

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* Jerome Chopard <revesansparole@gmail.com>

.. #}
