Overview
========

.. {# pkglts, glabpkg

.. image:: https://ecosensornet.gitlab.io/gateway/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://ecosensornet.gitlab.io/gateway/

.. image:: https://ecosensornet.gitlab.io/gateway/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/gateway/0.0.1/

.. image:: https://ecosensornet.gitlab.io/gateway/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/gateway

.. image:: https://badge.fury.io/py/gateway.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/gateway


.. #}

Gateway to receive messages from sensors
