============
Installation
============

If you only want to use the package::

    $ activate myenv
    (myenv) $ conda install gateway -c revesansparole
