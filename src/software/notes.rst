# software and installation on raspberry

sudo apt-get update
sudo apt-get install wget bzip2 ca-certificates curl make git


## install conda (from dockerfile in glabpkg)

https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html

ARCHI=aarch64
wget https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Linux-${ARCHI}.sh -O ~/miniconda.sh
sudo /bin/bash ~/miniconda.sh -b -p /opt/conda
sudo ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh
echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc

# create local env
conda create -n iot python=3.10 pyserial

echo "conda activate iot" >> ~/.bashrc


## install gateway from source

git clone https://gitlab.com/ecosensornet/gateway.git
cd gateway/
pip install -e .

esgate --help

# launch service rest
cp -R gateway/src/software/local_server_rest/* .
# from local_server_rest/readme.rst
sudo mv local_server_rest.service /etc/systemd/system/
sudo chmod 644 /etc/systemd/system/local_server_rest.service
sudo systemctl start local_server_rest
sudo systemctl enable local_server_rest


esgate listen -p /dev/ttyACM0
# start esgate as a service
cp gateway/src/software/esgate.service .
sudo mv esgate.service /etc/systemd/system/
sudo chmod 644 /etc/systemd/system/esgate.service
sudo systemctl start esgate
sudo systemctl enable esgate



# test
http://10.0.100.81:8020/menu.html
