Local server REST
=================

Simple server to manage interaction between computer and messages stored on gateway

ssh pi@192.168.230.1

How to deploy (see https://www.linode.com/docs/guides/start-service-at-boot/):
 - copy python files in/home/pi
 - copy *.service in /lib/systemd/system (sudo mv local_server_rest.service /etc/systemd/system/local_server_rest.service)
 - change permissions (sudo chmod 644 /etc/systemd/system/local_server_rest.service)
 - start service (sudo systemctl start local_server_rest)
 - ensure restart on startup (sudo systemctl enable local_server_rest)

connect to wifi network of pi and url:
http://192.168.230.1:8020
