function myFunction() {
    document.getElementById("demo").innerHTML = "Paragraph changed.";
    document.getElementById("first_date").value = "2021-04-11T00:00";
}

function last_day() {
    var datestr = new Date(Date.now() - 3.6e6 * 24).toISOString();
    document.getElementById("debug").innerHTML = datestr;
    document.getElementById("date_picker").value = datestr.substring(0, datestr.length - 1);
}
function last_week() {
    var datestr = new Date(Date.now() - 3.6e6 * 24 * 7).toISOString();
    document.getElementById("debug").innerHTML = datestr;
    document.getElementById("date_picker").value = datestr.substring(0, datestr.length - 1);
}
function last_month() {
    var datestr = new Date(Date.now() - 3.6e6 * 24 * 30).toISOString();
    document.getElementById("debug").innerHTML = datestr;
    document.getElementById("date_picker").value = datestr.substring(0, datestr.length - 1);
}


function set_now() {
    var datestr = new Date(Date.now()).toISOString();
    document.getElementById("debug").innerHTML = datestr;
    document.getElementById("date_picker").value = datestr.substring(0, datestr.length - 1);
//    document.getElementById("debug").innerHTML = new Date(Date.now()).toISOString();
}
