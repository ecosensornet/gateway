#pragma once

#include <esp_now.h>

#include "settings.h"

namespace mode_espnow {

void on_data_rcv(const uint8_t* mac, const uint8_t* data, int len);
void setup();
void loop();

}  // namespace mode_espnow
