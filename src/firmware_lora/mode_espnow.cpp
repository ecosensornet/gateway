#include <WiFi.h>

#include "mode_espnow.h"
#include "msg_queue.h"


void mode_espnow::on_data_rcv(const uint8_t* mac, const uint8_t* data, int len) {
  msg_queue::push((uint8_t)len, data);
#if (DEBUG == 1)
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("current stack size: ");
  Serial.println(msg_queue::length());
  Serial.print("esp: ");
  for (uint8_t i=0; i < len; i++) {
    Serial.print(data[i]);
  }
  Serial.println("");
#endif  // DEBUG
}


void mode_espnow::setup() {
#if (DEBUG == 1)
  Serial.println("setup local espnow");
#endif // DEBUG
  WiFi.mode(WIFI_STA);
  if (esp_now_init() != ESP_OK) {
#if (DEBUG == 1)
    Serial.println("Error initializing ESP-NOW");
#endif // DEBUG
    return;
  }

  esp_now_register_recv_cb(mode_espnow::on_data_rcv);
}


void mode_espnow::loop() {}
