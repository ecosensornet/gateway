#include <Arduino.h>
#include <lmic.h>
#include <hal/hal.h>

#include "board.h"
#include "loramac.h"
#include "msg_queue.h"

// Chose LSB mode on the console and then copy it here.
static const u1_t PROGMEM APPEUI[8] = { 0x31, 0x7A, 0x02, 0xD0, 0x7E, 0xD5, 0xB3, 0x71 };
// LSB mode
//static const u1_t PROGMEM DEVEUI[8] = { 0x2D, 0x13, 0xB2, 0x6C, 0xBE, 0x35, 0xE7, 0x01 };
static const u1_t PROGMEM DEVEUI[8] = { 0x2D, 0x13, 0xB2, 0x6C, 0xBE, 0x35, 0xE7, 0x02 };
// MSB mode
static const u1_t PROGMEM APPKEY[16] = {
  0x75, 0x7F, 0xE4, 0xB5, 0xD0, 0xBB, 0x39, 0x03, 0x9D, 0x24, 0x55, 0x35, 0xD8, 0x42, 0xCE, 0xD8
};

// Pin mapping
const lmic_pinmap lmic_pins = {
  .nss = RADIO_CS_PIN,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = RADIO_RST_PIN,
  .dio = { RADIO_DIO0_PIN, RADIO_DIO1_PIN, RADIO_BUSY_PIN }
};

static osjob_t sendjob;
static int spreadFactor = DR_SF7;
static int joinStatus = EV_JOINING;
static const unsigned TX_INTERVAL = 30;  // [s] interval between two messages

void os_getArtEui(u1_t *buf) {
  memcpy_P(buf, APPEUI, 8);
}

void os_getDevEui(u1_t *buf) {
  memcpy_P(buf, DEVEUI, 8);
}

void os_getDevKey(u1_t *buf) {
  memcpy_P(buf, APPKEY, 16);
}


void do_send(osjob_t *j) {
  if (joinStatus == EV_JOINING) {
#if (DEBUG == 1)
    Serial.println(F("Not joined yet"));
#endif  // DEBUG
    // Check if there is not a current TX/RX job running
    os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);

  } else if (LMIC.opmode & OP_TXRXPEND) {
#if (DEBUG == 1)
    Serial.println(F("OP_TXRXPEND, not sending"));
#endif  // DEBUG
  } else if (msg_queue::length() > 0) {
#if (DEBUG == 1)
    Serial.println(F("OP_TXRXPEND, sending ..."));
#endif  // DEBUG
    uint8_t payload[MAX_PAYLOAD_SIZE];
    uint8_t cur_ind = 0;
    uint8_t msg_len = msg_queue::peek_size(0);  // potential problem in arithmetic if max_payload_size is close to 256
    while (cur_ind + msg_len + 1 < MAX_PAYLOAD_SIZE) {
        payload[cur_ind] = msg_len;
        cur_ind ++;
        msg_len = msg_queue::pop(&(payload[cur_ind]));
        cur_ind = cur_ind + msg_len;
        if (msg_queue::length() > 0) {
            msg_len = msg_queue::peek_size(0);
        } else {
            msg_len = MAX_PAYLOAD_SIZE;
        }
    }
#if (DEBUG == 1)
    Serial.print("MSG: ");
    Serial.write(payload, cur_ind);
    Serial.println("");
#endif  // DEBUG
    // Prepare upstream data transmission at the next possible time.
    LMIC_setTxData2(1, payload, cur_ind, 0);
    os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
  } else {
#if (DEBUG == 1)
    Serial.println("msg queue empty");
#endif  // DEBUG
    os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
  }
}

void onEvent(ev_t ev) {
#if (DEBUG == 1)
  Serial.print(os_getTime());
  Serial.print(": ");
#endif  // DEBUG
  switch (ev) {
    case EV_TXCOMPLETE:
#if (DEBUG == 1)
      Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
#endif  // DEBUG

      if (LMIC.txrxFlags & TXRX_ACK) {
#if (DEBUG == 1)
        Serial.println(F("Received ack"));
#endif  // DEBUG
      }

      if (LMIC.dataLen) {
        // data received in rx slot after tx
#if (DEBUG == 1)
        Serial.print(F("Data Received: "));
        Serial.println(LMIC.dataLen);
        Serial.println(F(" bytes of payload"));
#endif  // DEBUG
      }
      // Schedule next transmission
      os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
      break;
    case EV_JOINING:
#if (DEBUG == 1)
      Serial.println(F("EV_JOINING: -> Joining..."));
#endif  // DEBUG
      joinStatus = EV_JOINING;
      break;
    case EV_JOIN_FAILED:
#if (DEBUG == 1)
      Serial.println(F("EV_JOIN_FAILED: -> Joining failed"));
#endif  // DEBUG
      break;
    case EV_JOINED:
#if (DEBUG == 1)
      Serial.println(F("EV_JOINED"));
#endif  // DEBUG
      joinStatus = EV_JOINED;

      delay(3);
      // Disable link check validation (automatically enabled
      // during join, but not supported by TTN at this time).
      LMIC_setLinkCheckMode(0);

      break;
    case EV_RXCOMPLETE:
      // data received in ping slot
#if (DEBUG == 1)
      Serial.println(F("EV_RXCOMPLETE"));
#endif  // DEBUG
      break;
    case EV_LINK_DEAD:
#if (DEBUG == 1)
      Serial.println(F("EV_LINK_DEAD"));
#endif  // DEBUG
      break;
    case EV_LINK_ALIVE:
#if (DEBUG == 1)
      Serial.println(F("EV_LINK_ALIVE"));
#endif  // DEBUG
      break;
    default:
#if (DEBUG == 1)
      Serial.println(F("Unknown event"));
#endif  // DEBUG
      break;
  }
}

void setupLMIC(void) {
  // LMIC init
  os_init();

  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();

  LMIC_setClockError(MAX_CLOCK_ERROR * 1 / 100);
  // Set up the channels used by the Things Network, which corresponds
  // to the defaults of most gateways. Without this, only three base
  // channels from the LoRaWAN specification are used, which certainly
  // works, so it is good for debugging, but can overload those
  // frequencies, so be sure to configure the full frequency range of
  // your network here (unless your network autoconfigures them).
  // Setting up channels should happen after LMIC_setSession, as that
  // configures the minimal channel set.

  LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);  // g-band
  LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7), BAND_CENTI);   // g-band
  LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK, DR_FSK), BAND_MILLI);    // g2-band
  // TTN defines an additional channel at 869.525Mhz using SF9 for class B
  // devices' ping slots. LMIC does not have an easy way to define set this
  // frequency and support for class B is spotty and untested, so this
  // frequency is not configured here.

  // Disable link check validation
  LMIC_setLinkCheckMode(0);

  // TTN uses SF9 for its RX2 window.
  LMIC.dn2Dr = DR_SF9;

  // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
  LMIC_setDrTxpow(spreadFactor, 14);

  // Start job
  LMIC_startJoining();


  do_send(&sendjob);  // Will fire up also the join
}

void loopLMIC(void) {
  os_runloop_once();
}
