#include <WiFi.h>

#include "board.h"
#include "loramac.h"
#include "mode_espnow.h"
#include "msg_queue.h"
#include "settings.h"

uint8_t maca[6];

void display_mac() {
  char buffer[30];
  sprintf(buffer,
          "MAC: %02x%02x%02x%02x%02x%02x",
          maca[0], maca[1], maca[2], maca[3], maca[4], maca[5]);
  Serial.println(buffer);
  sprintf(buffer,
          "MAC: % 4d% 4d% 4d% 4d% 4d% 4d",
          maca[0], maca[1], maca[2], maca[3], maca[4], maca[5]);
  Serial.println(buffer);
}


void setup() {
  board::setup();
  // Initialize Serial Monitor
#if (DEBUG == 1)
  Serial.begin(115200);
  while (!Serial) {
    delay(100);
  }
#endif  // DEBUG

  mode_espnow::setup();
  setupLMIC();

  WiFi.macAddress(maca);
#if (DEBUG == 1)
  display_mac();
#endif  // DEBUG
}

void loop() {
  board::signal_normal();
  mode_espnow::loop();
  loopLMIC();
#if (DEBUG == 1)
  if (Serial.available() > 0) {
    String cmd = Serial.readString();
    if (cmd == "mac") {
      display_mac();
    } else {
      Serial.print("msg not recognized as cmd: ");
      Serial.println(cmd);
    }
  }
#endif  // DEBUG
}
