#pragma once

#include "settings.h"

#ifndef LED_SIGNAL
#define LED_SIGNAL 37  // lilygo T3S3
#endif // LED_SIGNAL

#define I2C_SDA 18
#define I2C_SCL 17

#define RADIO_SCLK_PIN 5
#define RADIO_MISO_PIN 3
#define RADIO_MOSI_PIN 6
#define RADIO_CS_PIN 7
#define RADIO_DIO0_PIN 9
#define RADIO_DIO1_PIN 33
#define RADIO_DIO3_PIN 21
#define RADIO_DIO4_PIN 10
#define RADIO_DIO5_PIN 36
#define RADIO_BUSY_PIN 34
#define RADIO_RST_PIN 8


namespace board {

void setup();
void signal_normal();

}  // namespace board
