#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>

#include "board.h"

unsigned long last_blip;
unsigned long led_delay;

void board::setup() {
  pinMode(LED_SIGNAL, OUTPUT);
  last_blip = millis();
  digitalWrite(LED_SIGNAL, LOW);
  led_delay = 1000;

  // Lora transmitter
  Wire.begin(I2C_SDA, I2C_SCL);
  SPI.begin(RADIO_SCLK_PIN, RADIO_MISO_PIN, RADIO_MOSI_PIN);

}

void board::signal_normal(){
  if (millis() - last_blip > led_delay) {
    if (led_delay == 1000) {
      digitalWrite(LED_SIGNAL, HIGH);
      led_delay = 100;
    } else {
      digitalWrite(LED_SIGNAL, LOW);
      led_delay = 1000;
    }
    last_blip = millis();
  }
}
