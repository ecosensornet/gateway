#ifndef SETTINGS_H
#define SETTINGS_H

#include <Arduino.h>

#ifndef DEBUG
#define DEBUG 0
#endif // DEBUG

#ifndef LED_BUILTIN
#define LED_BUILTIN 15
#endif // LED_BUILTIN

#endif //  SETTINGS_H
