#include <esp_now.h>
#include <WiFi.h>

#include "settings.h"
#include "src/msg_queue.h"
#include "src/datetime.h"
#include "src/sdstore.h"

#define MODE_CONFIG 0
#define MODE_LISTEN 1
#define MODE_SD 2

uint8_t maca[6];

char msg[512];
char msg_out[5120];
char ts[20];

uint8_t mode;
bool sd_mounted;

void display_mac() {
  char buffer[18];
  sprintf(buffer,
          "MAC: %02x%02x%02x%02x%02x%02x",
          maca[0], maca[1], maca[2], maca[3], maca[4], maca[5]
         );
  Serial.println(buffer);
}


// Callback function that will be executed when data is received
void OnDataRecv(const uint8_t* mac, const uint8_t* data, int len) {
#if (DEBUG == 1)
  if (mode != MODE_SD) {
    Serial.print("Bytes received: ");
    Serial.println(len);
    Serial.print("JSON: ");
    Serial.println((char*) data);
  }
#endif // DEBUG
  if (mode == MODE_LISTEN) {
    msg_queue::push((char*) data);
#if (DEBUG == 1)
    Serial.println("pushed");
#endif // DEBUG
  }
}


void setup() {
  // Initialize Serial Monitor
  Serial.begin(115200);
  while (!Serial) {
    delay(100);
  }

  msg_queue::setup();
  datetime::setup();
  sd_mounted = sdstore::setup();
  if (!sd_mounted) {
    pinMode(LED_BUILTIN, OUTPUT);
    for (uint8_t i = 0; i < 10; i++) {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(100);
      digitalWrite(LED_BUILTIN, LOW);
      delay(200);
    }
    ESP.restart();
  }

  // internal mode of operation
  mode = MODE_CONFIG;

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  delay(2000);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
#if (DEBUG == 1)
    Serial.println("Error initializing ESP-NOW");
#endif // DEBUG
    pinMode(LED_BUILTIN, OUTPUT);
    for (uint8_t i = 0; i < 10; i++) {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(100);
      digitalWrite(LED_BUILTIN, LOW);
      delay(200);
    }
    return;
  }
  else {
    WiFi.macAddress(maca);
#if (DEBUG == 1)
    display_mac();
#endif // DEBUG
  }

  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);
}

void loop() {
  if (Serial.available() > 0) {
    String cmd = Serial.readString();
    if (cmd == "mac") {
      display_mac();
    }
    else if (cmd == "list_msg") {
      Serial.println(String("msg nb :") + String(msg_queue::length()));
      for (uint8_t i = 0; i < msg_queue::length(); i++) {
        msg_queue::peek(i, msg);
        Serial.println(String("msg ") + String(i) + String(" : "));
        Serial.println(msg);
      }
    }
    else if (cmd == "list") {
      if (mode != MODE_LISTEN) {
        Serial.println("list events");
        sdstore::list_events();
      }
      else {
        Serial.println("change mode first");
      }
    }
    else if (cmd == "ind") {
      if (mode != MODE_LISTEN) {
        Serial.print("last log ind: ");
        Serial.println(sdstore::last_log_index());
      }
      else {
        Serial.println("change mode first");
      }
    }
    else if (cmd == "mount") {
      sd_mounted = sdstore::mount();
      if (sd_mounted) {
        Serial.println("mounted");
      }
      else {
        Serial.println("unable to mount");
      }
    }
    else if (cmd == "unmount") {
      if (sdstore::unmount()) {
        Serial.println("SD unmounted");
        sd_mounted = false;
      }
      else {
        Serial.println("unable to unmount");
      }
    }
    else if (cmd == "usb") {
      if (mode != MODE_LISTEN) {
        sdstore::ser_events();
      }
      else {
        Serial.println("change mode first");
      }
    }
    else if (cmd == "clear") {
      if (mode != MODE_LISTEN) {
        Serial.println("clear events");
        sdstore::clear_events();
      }
      else {
        Serial.println("change mode first");
      }
    }
    else if (cmd == "ts") {
      Serial.print("Timestamp: ");
      datetime::ts_now(ts);
      Serial.println(ts);
    }
    else if (cmd.substring(0, 4) == "2023") {
      datetime::synchronize(cmd.c_str());
    }
    else if (cmd.substring(0, 5) == "mode_") {
      if (cmd.substring(5, 8) == "cfg") {
        mode = MODE_CONFIG;
      }
      else if (cmd.substring(5, 8) == "lst") {
        mode = MODE_LISTEN;
      }
      else if (cmd.substring(5, 7) == "sd") {
        mode = MODE_SD;
      }
      else {
        Serial.print("msg not recognized as mode: ");
        Serial.println(cmd);
      }
    }
    else {
      Serial.print("msg not recognized as cmd: ");
      Serial.println(cmd);
    }
  }
  else if (sd_mounted && msg_queue::length() > 0) {
    datetime::ts_now(ts);
    uint8_t queue_len = msg_queue::length();
    strcpy(msg_out, "");
    for (uint8_t i = 0; i < queue_len; i++) {
#if (DEBUG == 1)
      Serial.print("handle msg @ ");
      Serial.println(ts);
#endif // DEBUG
      strcpy(msg, ts);  // 19 characters
      strcat(msg, "->");  // 2 characters
      msg_queue::pop(&msg[21]);
      strcat(msg, "\n");
      strcat(msg_out, msg);
    }
    sdstore::store_event(msg_out);
  }
  else {
    delay(200);
  }
}
