#include <FS.h>
#include <SD.h>
#include <SPI.h>

#include "datetime.h"

#include "sdstore.h"

char root[] = "/events";
int32_t log_ind;  // need int since -1 used for initialization
char log_pth[256];
uint32_t entry_nb;
#define MAX_ENTRIES 1000

void createDir(fs::FS &fs, const char * path) {
#if (DEBUG == 1)
  Serial.printf("Creating Dir: %s\n", path);
#endif // DEBUG
  if (fs.mkdir(path)) {
#if (DEBUG == 1)
    Serial.println("Dir created");
#endif // DEBUG
  }
  else {
#if (DEBUG == 1)
    Serial.println("mkdir failed");
#endif // DEBUG
  }
}


bool sdstore::mount(){
  if (!SD.begin()) {
#if (DEBUG == 1)
    Serial.println("Card Mount Failed");
#endif // DEBUG
    return false;
  }
  uint8_t cardType = SD.cardType();
#if (DEBUG == 1)
    Serial.print("SD card type: ");
    Serial.println(cardType);
#endif // DEBUG

  if (cardType == CARD_NONE) {
#if (DEBUG == 1)
    Serial.println("No SD card attached");
#endif // DEBUG
    return false;
  }

  // create events directory
  if (!SD.exists(root)) {
    createDir(SD, root);
  }
#if (DEBUG == 1)
  Serial.println("root created");
#endif // DEBUG

  // increase current_ind until free (count files?)

  return true;
}


bool sdstore::unmount() {
    // calling this function ensure no other file is open since no parallel computing
    SD.end();
    return true;
}


bool sdstore::setup() {
    bool sd_mounted = mount();
    log_ind = last_log_index();
    increase_log();
    return sd_mounted;
}


int32_t sdstore::last_log_index() {
  int32_t ind_max = -1;
  File root_fld = SD.open(root);
  if (!root_fld) {
    Serial.println("Failed to open directory");
    return -1;
  }
  if (!root_fld.isDirectory()) {
    Serial.println("Root is not a directory");
    return -1;
  }

  File log = root_fld.openNextFile();
  int32_t ind;
  uint8_t i;
  while (log) {
    ind = 0;
    for (i=4;i<8;i++) {
        ind = ind * 10 + (int32_t)(log.name()[i] - '0');
    }
    if (ind > ind_max){
      ind_max=ind;
    }
    log = root_fld.openNextFile();
  }
  root_fld.close();

  return ind_max;
}


void sdstore::increase_log() {
    log_ind ++;
    entry_nb = 0;
    sprintf(log_pth, "%s/log_%04d.json", root, log_ind);
}


void sdstore::store_event(const char* msg){
#if (DEBUG == 1)
  Serial.print("append event to: ");
  Serial.println(log_pth);
#endif // DEBUG

  File file = SD.open(log_pth, FILE_APPEND);
  if (!file) {
#if (DEBUG == 1)
    Serial.println("Failed to open file for appending");
#endif // DEBUG
    return;
  }
  if (file.print(msg)) {
#if (DEBUG == 1)
    Serial.println("File written");
#endif // DEBUG
  }
  else {
#if (DEBUG == 1)
    Serial.println("Write failed");
#endif // DEBUG
 }
  file.close();
  entry_nb ++;
  if (entry_nb > MAX_ENTRIES) {
    increase_log();
  }
}

void sdstore::list_events() {
  File root_fld = SD.open(root);
  if (!root_fld) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root_fld.isDirectory()) {
    Serial.println("Root is not a directory");
    return;
  }

  File log = root_fld.openNextFile();
  while (log) {
    Serial.println(log.name());
    log = root_fld.openNextFile();
  }
  root_fld.close();
}


void sdstore::clear_events() {
  File root_fld = SD.open(root);
  if (!root_fld) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root_fld.isDirectory()) {
    Serial.println("Root is not a directory");
    return;
  }

  char full_pth[512];
  File log = root_fld.openNextFile();
  while (log) {
      sprintf(full_pth, "%s/%s", root, log.name());
      Serial.print("RM: ");
      Serial.println(full_pth);
      if (SD.remove(full_pth)) {
        Serial.println("removed");
      }
      else {
        Serial.println("rm failed");
      }
      log = root_fld.openNextFile();
  }
  root_fld.close();

  log_ind = -1;
  increase_log();
}


void sdstore::ser_events() {
  File root_fld = SD.open(root);
  if (!root_fld) {
    Serial.println("Failed to open directory");
    return;
  }
  if (!root_fld.isDirectory()) {
    Serial.println("Root is not a directory");
    return;
  }

  char full_pth[512];
  File log = root_fld.openNextFile();
  Serial.println("SER BEGIN");
  while (log) {
      sprintf(full_pth, "%s/%s", root, log.name());
      Serial.print("PTH: ");
      Serial.println(full_pth);
      while (log.available()) {
        Serial.write(log.read());
        }
      Serial.println("");
      log = root_fld.openNextFile();
  }
  root_fld.close();
  Serial.println("SER END");
}

