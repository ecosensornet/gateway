#include <Arduino.h>
#include <TimeLib.h>

#include "datetime.h"

void datetime::setup(){
#if (DEBUG == 1)
    Serial.println("datetime setup");
#endif // DEBUG
}

// e.g. 2023-03-24T13:58:50
// py: datetime.utcnow().isoformat()
void datetime::synchronize(const char* ts){
#if (DEBUG == 1)
    Serial.print("datetime synchronize @ ");
    Serial.println(ts);
#endif // DEBUG
    // parse timestamp
    int year = 0;
    for (int i=0;i<4;i++){
        year = (year * 10) + (ts[i] - '0');
    }
    int month = 0;
    for (int i=5;i<7;i++){
        month = (month * 10) + (ts[i] - '0');
    }
    int day = 0;
    for (int i=8;i<10;i++){
        day = (day * 10) + (ts[i] - '0');
    }

    int hour = 0;
    for (int i=11;i<13;i++){
        hour = (hour * 10) + (ts[i] - '0');
    }
    int minute = 0;
    for (int i=14;i<16;i++){
        minute = (minute * 10) + (ts[i] - '0');
    }
    int second = 0;
    for (int i=17;i<19;i++){
        second = (second * 10) + (ts[i] - '0');
    }

    // update now
    setTime(hour, minute, second, day, month, year);
}
void datetime::ts_now(char* ts, bool pth_safe){
  time_t t = now(); // Store the current time in time
  if (pth_safe) {
    sprintf(ts,
          "%04d%02d%02dT%02d%02d%02d",
          year(t), month(t), day(t), hour(t), minute(t), second(t)
         );
         }
  else {
    sprintf(ts,
          "%04d-%02d-%02dT%02d:%02d:%02d",
          year(t), month(t), day(t), hour(t), minute(t), second(t)
         );
         }
}
