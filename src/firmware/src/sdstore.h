#ifndef SDSTORE_H
#define SDSTORE_H

#include "../settings.h"

namespace sdstore {

bool setup();
bool mount();
bool unmount();
int32_t last_log_index();
void increase_log();
void store_event(const char* msg);
void list_events();
void clear_events();
void ser_events();

}

#endif //  SDSTORE_H
