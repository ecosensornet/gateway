#ifndef MSG_QUEUE_H
#define MSG_QUEUE_H

#include "../settings.h"

namespace msg_queue {

void setup();
uint8_t length();
void push(const char* msg);
void peek(uint8_t ind, char* msg);
void pop(char* msg);

}

#endif //  MSG_QUEUE_H
