#include "msg_queue.h"

#define BUFFER_SIZE 10

uint8_t current_head = 0;  // current position of first written message
uint8_t current_tail = 0;  // current position were to write next message
char msgs[BUFFER_SIZE][512];

void msg_queue::setup(){
#if (DEBUG == 1)
  Serial.println("setup queue");
#endif  // DEBUG
    current_head = 0;
    current_tail = 0;
}

uint8_t msg_queue::length() {
    if (current_tail >= current_head) {
      return current_tail - current_head;
    }
    else {
      return BUFFER_SIZE - current_head + current_tail;
    }
}

void msg_queue::push(const char* msg){
  strcpy(msgs[current_tail], msg);
  current_tail ++;
  if (current_tail == BUFFER_SIZE) {
    current_tail = 0;
  }
  if (current_tail == current_head) {
#if (DEBUG == 1)
    Serial.println("overflow");
#endif  // DEBUG
    // rollback current_tail (will erase last message potentially next push)
    if (current_tail == 0) {
      current_tail = BUFFER_SIZE - 1;
    }
    else {
      current_tail --;
    }
  }
}

void msg_queue::peek(uint8_t ind, char* msg) {
    if (ind >= length()) {
#if (DEBUG == 1)
    Serial.println("IndexError");
#endif  // DEBUG
    }
    else {
        ind = current_head + ind;
        if (ind >= BUFFER_SIZE) {
          ind = ind - BUFFER_SIZE;
        }
        strcpy(msg, msgs[ind]);
    }

}
void msg_queue::pop(char* msg) {
    if (current_head == current_tail) {
#if (DEBUG == 1)
    Serial.println("empty");
#endif  // DEBUG
        return;
    }
  strcpy(msg, msgs[current_head]);
  current_head ++;
  if (current_head == BUFFER_SIZE) {
    current_head = 0;
  }
}
