#ifndef DATETIME_H
#define DATETIME_H

#include "../settings.h"

namespace datetime {

void setup();
void synchronize(const char* ts);
void ts_now(char* ts, bool pth_safe=false);

}

#endif //  DATETIME_H
