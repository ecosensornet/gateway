from datetime import datetime
from time import sleep
from pathlib import Path

import serial


def open_port(port):
    """Open serial port for reading

    Args:
        port: name of serial port

    Returns:
        (serial object): already open
    """
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = 115200
    ser.timeout = 1  # [s]
    ser.rts = False
    # ser.dtr = False
    ser.open()  # need to do it exactly like that to ensure no reset signal is send

    return ser


def ask_mac(port):
    """Ask for MAC address on given port

    Args:
        port (str): name of serial port

    Returns:
        None
    """
    ser = open_port(port)
    ser.write(b'mac')

    while True:
        line = ser.readline()
        if line:
            print(f"msg ({datetime.now().time().isoformat()}): {line}")
            if line.startswith(b"MAC:"):
                mac_hex = line[5:].strip()
                mac_dec = [int(mac_hex[i * 2:i * 2 + 2], 16) for i in range(6)]
                print("dec", mac_dec)
                return


def synchronize(port):
    ser = open_port(port)
    ts = datetime.utcnow().isoformat()
    ts = ts.split('.')[0]
    ser.write(ts.encode('utf-8'))
    sleep(0.3)
    line = ser.readline()
    while line:
        print("line", line)
        line = ser.readline()


def change_mode(mode, port):
    ser = open_port(port)
    ser.write(f'mode_{mode}'.encode('utf-8'))
    sleep(0.3)
    line = ser.readline()
    while line:
        print("buffered line", line)
        line = ser.readline()


def load_events(port):
    change_mode('sd', port)

    ser = open_port(port)
    ser.write(b'usb')
    print("usb sent")

    pth_cur = "events/dummy.json"

    while True:
        line = ser.readline()
        if line:
            print(line)
            if line.startswith(b"PTH: "):
                pth_cur = line[5:].strip().decode('utf-8')
                if pth_cur.startswith("/"):
                    pth_cur = pth_cur[1:]
                pth_cur = Path(pth_cur)
            if line.startswith(b"JSON: "):
                pth_cur.parent.mkdir(parents=True, exist_ok=True)
                pth_cur.write_bytes(line[5:].strip())
            if line.startswith(b"SER END"):
                return


def clear_events(port):
    ser = open_port(port)
    ser.write(b'clear')
    sleep(0.3)
    line = ser.readline()
    while line:
        print("buffered line", line)
        line = ser.readline()


def list_events(port):
    ser = open_port(port)
    ser.write(b'list')
    sleep(0.3)
    line = ser.readline()
    while line:
        print("line", line)
        line = ser.readline()
