"""
Listen on specific serial port to retrieve messages
"""
from datetime import datetime
from pathlib import Path

import node_base.sample
import node_light.sample
import node_multitemp.sample
import serial

factory = {pkg.Sample.iid: pkg.Sample for pkg in [node_base.sample, node_light.sample, node_multitemp.sample]}


def task_save_event(post_data, pth_log):
    """Write event on disk

    Args:
        post_data (bytes): string
        pth_log (Path): log file to store events

    Returns:
        None
    """
    now = datetime.utcnow()
    with open(pth_log, 'a') as fhw:
        print(f"{now.isoformat()}->{post_data}\n")
        fhw.write(f"{now.isoformat()}->{post_data}\n")
        # fhw.write(f"{now.isoformat()}->{post_data.decode('utf-8').strip()}\n")


def open_port(port):
    """Open serial port for reading

    Args:
        port: name of serial port

    Returns:
        (serial object): already open
    """
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = 115200
    ser.timeout = 1  # [s]
    # ser.rts = False
    # ser.dtr = False
    ser.open()  # need to do it exactly like that to ensure no reset signal is send

    return ser


def ask_mac(port):
    """Ask for MAC address on given port

    Args:
        port (str): name of serial port

    Returns:
        None
    """
    ser = open_port(port)
    ser.write(b'mac')

    while True:
        line = ser.readline()
        if line:
            print(f"msg ({datetime.now().time().isoformat()}): {line}")
            if line.startswith(b"MAC:"):
                mac_hex = line[5:].strip()
                mac_dec = [int(mac_hex[i * 2:i * 2 + 2], 16) for i in range(6)]
                print("dec", mac_dec)
                return


def read_port(port):
    """Read serial on given port

    Warnings:
        This function is permanently blocking

    Args:
        port (str): name of serial port

    Returns:
        None
    """
    ser = open_port(port)

    while True:
        line = ser.readline()
        if line:
            print(f"msg ({datetime.utcnow().time().isoformat()}): {line}")


def listen_on_port(port, repo_fld):
    """Launch gateway on given port

    Warnings:
        This function is permanently blocking

    Args:
        port (str): name of serial port
        repo_fld (Path): base dir to store events

    Returns:
        None
    """
    ser = open_port(port)
    log_ind = 0
    pth_log = repo_fld / f"log_{log_ind:04d}.log"
    entry_nb = 0

    while True:
        line = ser.readline()
        if line:
            if line.startswith(b"MSG:"):
                buffer = line[5:].strip()
                task_save_event(buffer, pth_log)
                entry_nb += 1

                # attempt to decode for debugging purpose
                try:
                    buffer = eval(buffer)
                    nid = "".join(f"{v:02X}" for v in buffer[:6])
                    payload = buffer[6:]
                    fac = None
                    try:
                        fac = factory[payload[0]]
                    except IndexError:
                        print("bad sample, empty")
                    except KeyError:
                        print("unrecognized sample IID", payload)

                    if fac is not None:
                        sample = fac()
                        sample.decode(payload)
                        print(f"FROM: {nid}")
                        print(sample.to_json())

                except SyntaxError:
                    print("bad syntax", buffer)
            else:
                print(f"msg ({datetime.utcnow().time().isoformat()}): {line}")
