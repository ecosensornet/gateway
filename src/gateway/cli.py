"""
Command line for gateway to receive messages from sensors
"""
from argparse import ArgumentParser
from pathlib import Path

from serial.tools.list_ports import comports

from .listener import ask_mac, listen_on_port, read_port


def action_ls(**kwds):
    """List available serial ports
    """
    for port in comports():
        print("port", port.name)


def action_mac(**kwds):
    """Ask for getaway MAC address
    """
    ask_mac(kwds['port'])


def action_clear(**kwds):
    """Clear all events
    """
    if kwds['force'] or (input(f"clear all events from repo y, [n]?") == "y"):
        repo_fld = Path("events")
        for node_dir in repo_fld.glob("node_*"):
            for pth in node_dir.glob("event_*.json"):
                pth.unlink()


def action_read(**kwds):
    """Read serial port
    """
    read_port(kwds['port'])


def action_listen(**kwds):
    """Listen for incoming events
    """
    repo_fld = Path("events")
    repo_fld.mkdir(exist_ok=True)

    listen_on_port(kwds['port'], repo_fld)


def main():
    """Run CLI evaluation"""
    action = dict(
        mac=action_mac,
        ls=action_ls,
        clear=action_clear,
        read=action_read,
        listen=action_listen,
    )
    parser = ArgumentParser(description='Gateway for ecosensornet')
    parser.add_argument("-v", "--verbosity", action="count", default=0,
                        help="increase output verbosity")

    subparsers = parser.add_subparsers(dest='subcmd', help='sub-command help')

    parser_ls = subparsers.add_parser('ls', help=action_ls.__doc__)

    parser_mac = subparsers.add_parser('mac', help=action_mac.__doc__)
    parser_mac.add_argument("-p", "--port", action='store',
                            help="Serial port to find gateway")

    parser_clear = subparsers.add_parser('clear', help=action_clear.__doc__)
    parser_clear.add_argument("-f", "--force", action='store_true',
                              help="Force clear without confirmation")

    parser_listen = subparsers.add_parser('read', help=action_read.__doc__)
    parser_listen.add_argument("-p", "--port", action='store',
                               help="Serial port to listen to")

    parser_listen = subparsers.add_parser('listen', help=action_listen.__doc__)
    parser_listen.add_argument("-p", "--port", action='store',
                               help="Serial port to listen to")

    kwds = vars(parser.parse_args())
    verbosity = kwds.pop('verbosity')

    # perform action
    subcmd = kwds.pop('subcmd')
    action[subcmd](**kwds)


if __name__ == '__main__':
    main()
