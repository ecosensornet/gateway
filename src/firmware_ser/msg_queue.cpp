#include "msg_queue.h"


uint8_t current_head = 0;  // current position of first written message
uint8_t current_tail = 0;  // current position were to write next message
uint8_t msg_sizes[BUFFER_SIZE];
uint8_t msgs[BUFFER_SIZE][MAX_MSG_SIZE];

void msg_queue::setup() {
#if (DEBUG == 1)
  Serial.println("setup queue");
#endif  // DEBUG
  current_head = 0;
  current_tail = 0;
}

uint8_t msg_queue::length() {
  if (current_tail >= current_head) {
    return current_tail - current_head;
  } else {
    return BUFFER_SIZE - current_head + current_tail;
  }
}

void msg_queue::push(uint8_t size, const uint8_t* msg) {
  memcpy(msgs[current_tail], msg, size);
  msg_sizes[current_tail] = size;
  current_tail++;
  if (current_tail == BUFFER_SIZE) {
    current_tail = 0;
  }
  if (current_tail == current_head) {
#if (DEBUG == 1)
    Serial.println("overflow");
#endif  // DEBUG
    // rollback current_tail (will erase last message potentially next push)
    if (current_tail == 0) {
      current_tail = BUFFER_SIZE - 1;
    } else {
      current_tail--;
    }
  }
}

uint8_t msg_queue::peek(uint8_t ind, uint8_t* msg) {
  uint8_t size = 0;
  if (ind >= length()) {
#if (DEBUG == 1)
    Serial.println("IndexError");
#endif  // DEBUG
  } else {
    ind = current_head + ind;
    if (ind >= BUFFER_SIZE) {
      ind = ind - BUFFER_SIZE;
    }
    size = msg_sizes[ind];
    memcpy(msg, msgs[ind], size);
    // strcpy(msg, msgs[ind]);
  }
  return size;
}

uint8_t msg_queue::pop(uint8_t* msg) {
  if (current_head == current_tail) {
#if (DEBUG == 1)
    Serial.println("empty");
#endif  // DEBUG
    return 0;
  }
  uint8_t size = msg_sizes[current_head];
  memcpy(msg, msgs[current_head], size);
  current_head++;
  if (current_head == BUFFER_SIZE) {
    current_head = 0;
  }
  return size;
}
