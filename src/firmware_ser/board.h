#pragma once

#include "settings.h"

#ifndef LED_SIGNAL
#define LED_SIGNAL 15  // lolin S2 mini
#endif // LED_SIGNAL

namespace board {

void setup();
void signal_normal();

}  // namespace board
