#include <WiFi.h>

#include "board.h"
#include "mode_espnow.h"
#include "msg_queue.h"
#include "settings.h"

uint8_t maca[6];

void display_mac() {
  char buffer[30];
  sprintf(buffer,
          "MAC: %02x%02x%02x%02x%02x%02x",
          maca[0], maca[1], maca[2], maca[3], maca[4], maca[5]);
  Serial.println(buffer);
  sprintf(buffer,
          "MAC: % 4d% 4d% 4d% 4d% 4d% 4d",
          maca[0], maca[1], maca[2], maca[3], maca[4], maca[5]);
  Serial.println(buffer);
}


void setup() {
  board::setup();
  // Initialize Serial Monitor
  Serial.begin(115200);
  while (!Serial) {
    delay(100);
  }

  mode_espnow::setup();

  WiFi.macAddress(maca);
#if (DEBUG == 1)
  display_mac();
#endif  // DEBUG
}

void loop() {
  board::signal_normal();
  if (Serial.available() > 0) {
    String cmd = Serial.readString();
    if (cmd == "mac") {
      display_mac();
    } else {
      Serial.print("msg not recognized as cmd: ");
      Serial.println(cmd);
    }
  } else if (msg_queue::length() > 0) {
    uint8_t msg_data[MAX_MSG_SIZE];
    uint8_t msg_len = msg_queue::pop(msg_data);
    Serial.print("MSG: [");
    for (uint8_t i = 0; i < msg_len; i++) {
      Serial.print(msg_data[i]);
      Serial.print(", ");
    }
    Serial.println("]");
  } else {
    mode_espnow::loop();
  }
}
