#include <WiFi.h>

#include "mode_espnow.h"
#include "msg_queue.h"


void mode_espnow::on_data_rcv(const uint8_t* mac, const uint8_t* data, int len) {
  uint8_t* msg = new uint8_t[len + 6];
  memcpy(msg, mac, 6);
  memcpy(&msg[6], data, len);

  msg_queue::push((uint8_t)(len + 6), msg);
#if (DEBUG == 1)
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("from: ");
  for (uint8_t i = 0; i < 6; i++) {
    Serial.print(mac[i]);
    Serial.print(", ");
  }
  Serial.println("");
  Serial.print("current stack size: ");
  Serial.println(msg_queue::length());
#endif  // DEBUG
}


void mode_espnow::setup() {
#if (DEBUG == 1)
  Serial.println("setup local espnow");
#endif  // DEBUG
  WiFi.mode(WIFI_STA);
  if (esp_now_init() != ESP_OK) {
#if (DEBUG == 1)
    Serial.println("Error initializing ESP-NOW");
#endif  // DEBUG
    return;
  }

  esp_now_register_recv_cb(mode_espnow::on_data_rcv);
}


void mode_espnow::loop() {}
