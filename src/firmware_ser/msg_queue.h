#pragma once

#include <Arduino.h>

#include "settings.h"

#define BUFFER_SIZE 10  // smaller than 256
#define MAX_MSG_SIZE 256

namespace msg_queue {

void setup();
uint8_t length();
void push(uint8_t size, const uint8_t* msg);
uint8_t peek(uint8_t ind, uint8_t* msg);
uint8_t pop(uint8_t* msg);

}
